USE northwind;

SHOW TABLES;
-- no 1
SELECT FirstName,LastName, City  FROM employees WHERE City = "Seattle" or City="London" order by City;

-- no 2 
SELECT suppliers.CompanyName, products.ProductName FROM products
JOIN suppliers ON suppliers.SupplierID = products.SupplierID 
WHERE products.ProductName = "Chocolade";

-- no 3
SELECT * FROM customers;
SELECT * FROM shippers;
SELECT * FROM categories;
SELECT * FROM territories;
SELECT * FROM customercustomerdemo;
SELECT * FROM orders;
SELECT * FROM suppliers;
SELECT * FROM products;

-- SELECT customers.ContactName FROM customers
-- JOIN shippers O

SELECT customers.ContactName, shippers.CompanyName FROM customers
JOIN orders ON orders.CustomerID = customers.CustomerID
JOIN shippers ON orders.ShipVia = shippers.ShipperID WHERE shippers.CompanyName = "Speedy Express";


-- no 4
SELECT categories.CategoryName, suppliers.CompanyName FROM categories
JOIN products ON  products.CategoryID = categories.CategoryID
JOIN suppliers ON products.SupplierID = suppliers.SupplierID WHERE suppliers.CompanyName = "Tokyo Traders";


-- no 5
SELECT products.ProductName, customers.ContactName FROM products
JOIN orders ON orders.CustomerID = customer.CustomerID 
JOIN orderdetails ON order

