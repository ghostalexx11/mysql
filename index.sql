-- No 1
CREATE DATABASE Perpustakaan_21103041078;
-- No 2
USE Perpustakaan_21103041078;

-- No 3
CREATE TABLE Buku (
	`Kode_Buku` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `Judul_Buku` VARCHAR(50) NOT NULL,
	`Tipe_Buku` VARCHAR(20) NOT NULL,
	`Kode_Pengarang` INT NOT NULL,
    `Tahun_Terbit` DATETIME NOT NULL,
    `Nama_Penerbit` VARCHAR(20) NOT NULL
);


CREATE TABLE Pengarang (
	`Kode_Pengarang` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `Nama_Depan` VARCHAR(20) NOT NULL,
	`Nama_Belakang` VARCHAR(20) NOT NULL,
    `Alamat` VARCHAR(20) NOT NULL,
    `Kota` VARCHAR(20) NOT NULL,
	`Telepon` VARCHAR(15) NOT NULL
);

CREATE TABLE Pinjam (
	`Kode_Buku` INT NOT NULL PRIMARY KEY,
    `Kode_Peminjam` INT NOT NULL,
    `Tanggal_Pinjam` DATETIME
);

CREATE TABLE Peminjam (
	`Kode_Peminjam` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `Nama_Depan_Pinjam` VARCHAR(20) NOT NULL,
	`Nama_Belakang_Pinjam` VARCHAR(20) NOT NULL,
    `Alamat` VARCHAR(20) NOT NULL,
    `Kota` VARCHAR(20) NOT NULL,
	`Telepon` VARCHAR(15) NOT NULL
);
 
 
ALTER TABLE Buku ADD FOREIGN KEY(Kode_Pengarang) REFERENCES Pengarang(Kode_Pengarang);
ALTER TABLE Pinjam ADD FOREIGN KEY(Kode_Buku) REFERENCES Buku(Kode_Buku);
ALTER TABLE Pinjam ADD FOREIGN KEY(Kode_Peminjam) REFERENCES Peminjam(Kode_Peminjam);



DROP DATABASE Perpustakaan_21103041078;
