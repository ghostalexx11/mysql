USE order_entry_21103041078;

SHOW TABLES;

DESC customers;

SELECT vend_name, vend_city FROM vendors WHERE vend_city = "London";

SELECT cust_name, cust_city FROM customers WHERE cust_city="Chicago"; 

SELECT * FROM vendors;


-- with JOIN
SELECT customers.cust_id, o.order_num , customers.cust_name, o.order_date 
FROM orders as o 
INNER JOIN customers ON o.cust_id = customers.cust_id WHERE customers.cust_id = "10001";


-- with JOIN pak arif
SELECT customers.cust_id, o.order_num , customers.cust_name, o.order_date
FROM orders as o
INNER JOIN customers ON o.cust_id = customers.cust_id;

-- 2
-- with JOIN
SELECT customers.cust_id, o.order_num , customers.cust_name, o.order_date 
FROM orders as o 
INNER JOIN customers ON o.cust_id = customers.cust_id WHERE customers.cust_name = "Yosemite Place";

-- 3 INNER JOIN 3 table
SELECT c.cust_name, o.order_date, oi_prod_id, oi_prod_quantity
FROM customers c JOIN orders o
ON c.cust.id = o.cust_id JOIN orderitems oi
ON o.order_num = oi.order_num WHERE c.cust_name = 'Yosemite Place';


-- WHERE customers.cust_id = "10001";
-- pak arif

SELECT vendors.vend_name, products.prod_name FROM vendors, products WHERE vendors.vend_id = products.vend_id;

-- alias
SELECT v.vend_name, p.prod_name FROM vendors as v, products as p WHERE v.vend_id = p.vend_id;
