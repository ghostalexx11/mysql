-- mengaktifkan database
USE praktikum_ghifar;

-- melihat struktur sebuah tabel
DESC mahasiswa;

-- menambah data sebuah tabel?
INSERT INTO mahasiswa VALUES('5565645', '22425556', 'Pecco', 'Ducati Lenovo', 'Motogp');
INSERT INTO mahasiswa VALUES('655669', '22425557', 'Jack Miller', 'Ducati Lenovo', 'Motogp');
INSERT INTO mahasiswa(Nim_Mhs, Nama_Mhs, Jurusan, Fakultas, Alamat) VALUES ('2242556', 'Alvaro Baustista', 'Arubatic Inc', 'Wsbk', 'Spanyol');

-- dengan sintaks lain
INSERT INTO mahasiswa (Nim_Mhs, Nama_Mhs, Jurusan, Fakultas)
VALUES('22425557', 'Jack Miller', 'Ducati Lenovo', 'Motogp');

-- select all *
SELECT * FROM mahasiswa;

-- select spesifik
SELECT Nim_Mhs, Nama_Mhs FROM mahasiswa;

-- query spesifik
SELECT Nim_Mhs, Nama_Mhs FROM mahasiswa WHERE Nim_Mhs="22425557";

-- query spesifik dua 
SELECT Nim_Mhs, Nama_Mhs, Fakultas FROM mahasiswa WHERE Fakultas="Wsbk" OR Fakultas="Motogp";

-- query spesifik dua required 
SELECT Nim_Mhs, Nama_Mhs, Fakultas FROM mahasiswa WHERE Nama_Mhs="Pecco"  AND Fakultas="Motogp" ;

-- update dengan kondisi
SELECT Nim_Mhs, Nama_Mhs FROM mahasiswa WHERE Nim <= "1";

-- Update Table
UPDATE mahasiswa SET Alamat = 'Australia' WHERE Nim_Mhs='22425557';

-- add table coluns
ALTER TABLE mahasiswa ADD COLUMN Alamat VARCHAR(80) NOT NULL AFTER Fakultas; 

SHOW TABLES;

-- Delete columns data
DELETE FROM mahasiswa WHERE ID="596569";

-- Urutan data mengunakan Asc dan Desc
SELECT * FROM mahasiswa ORDER BY Nama_Mhs ASC;










-- create mhs
CREATE TABLE mhs(
	`Nim` char(8) PRIMARY KEY,
    `Nama` VARCHAR(50) NOT NULL,
    `Alamat` VARCHAR(75) NOT NULL
);

-- melihat struktur sebuah tabel
DESC mhs;

-- memasukan nilai
INSERT INTO mhs VALUES("3", "Augusto Fernandez", "Spanyol");

-- mengambil semua
SELECT * FROM mhs;

-- update table 
UPDATE mhs SET Alamat = 'Italia' WHERE Nim='1';

-- update dengan kondisi
SELECT Nim, Nama FROM mhs WHERE Nim >= "2";




