-- Rename Columns
ALTER TABLE <table_name>
RENAME COLUMN <old_name> TO <new_name>;

-- Add Values Columns
INSERT INTO mahasiswa VALUES('655669', '22425557', 'Jack Miller', 'Ducati Lenovo', 'Motogp');
INSERT INTO mahasiswa(Nim_Mhs, Nama_Mhs, Jurusan, Fakultas, Alamat) VALUES ('2242556', 'Alvaro Baustista', 'Arubatic Inc', 'Wsbk', 'Spanyol');

-- select all *
SELECT * FROM mahasiswa;

-- select spesifik
SELECT Nim_Mhs, Nama_Mhs FROM mahasiswa;

-- query spesifik
SELECT Nim_Mhs, Nama_Mhs FROM mahasiswa WHERE Nim_Mhs="22425557";

-- update dengan kondisi
SELECT Nim_Mhs, Nama_Mhs FROM mahasiswa WHERE Nim <= "1";


